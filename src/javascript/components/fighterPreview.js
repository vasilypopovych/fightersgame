import { createElement } from '../helpers/domHelper';


export function createFighterPreview(fighter, position) {

  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });
  // todo: show fighter info (image, name, health, etc.) (DONE)
  
  
  const fighterInfo = `<strong><ul>
  <li>Name: ${fighter.name}</li>
  <li>Health: ${fighter.health}</li>
  <li>Attack: ${fighter.attack}</li>
  <li>Defense: ${fighter.defense}</li>
  <img src="${fighter.source}" height="150px">
</ul></strong>`
  
  fighterElement.innerHTML = fighterInfo;

  return fighterElement;

}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}

