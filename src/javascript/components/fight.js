import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  let firstFighterHealthBar = document.querySelector('#left-fighter-indicator');
  let secondFighterHealthBar = document.querySelector('#right-fighter-indicator');
  let pressedKeys = new Set();
  let switchCriticalHitFirstFighter = true;
  let switchCriticalHitSecondFighter = true;
  let firstFighterHealth = 100;
  let secondFighterHealth = 100;

  document.addEventListener('keydown', (event) => {
    pressedKeys.add(event.code);
    if (
      pressedKeys.has(controls.PlayerOneAttack) &&
      !pressedKeys.has(controls.PlayerTwoBlock) &&
      !pressedKeys.has(controls.PlayerOneBlock)
    ) {
      let damage = getDamage(firstFighter, secondFighter);
      let damageInterest = countDamageInterest(secondFighter, damage);
      //перевірка чи у змінній кількість здоровя бійця не стала мінусовою.
      if (secondFighterHealth < 0) {
        secondFighterHealthBar.style.width = '0%';
      } else {
        secondFighterHealthBar.style.width = `${(secondFighterHealth -= damageInterest)}%`;
      }
    } else if (
      pressedKeys.has(controls.PlayerTwoAttack) &&
      !pressedKeys.has(controls.PlayerOneBlock) &&
      !pressedKeys.has(controls.PlayerTwoBlock)
    ) {
      let damage = getDamage(secondFighter, firstFighter);
      let damageInterest = countDamageInterest(firstFighter, damage);
      if (firstFighterHealth < 0) {
        firstFighterHealthBar.style.width = '0%';
      } else {
        firstFighterHealthBar.style.width = `${(firstFighterHealth -= damageInterest)}%`;
      }
      // критичні удари
    } else if (
      pressedKeys.has(controls.PlayerOneCriticalHitCombination[0]) &&
      pressedKeys.has(controls.PlayerOneCriticalHitCombination[1]) &&
      pressedKeys.has(controls.PlayerOneCriticalHitCombination[2]) &&
      switchCriticalHitFirstFighter
    ) {
      let damage = getDamage(firstFighter, secondFighter) * 2;
      let damageInterest = countDamageInterest(secondFighter, damage);
      //перевірка чи у змінній кількість здоровя бійця не стала мінусовою.
      if (secondFighterHealth < 0) {
        secondFighterHealthBar.style.width = '0%';
      } else {
        secondFighterHealthBar.style.width = `${(secondFighterHealth -= damageInterest)}%`;
      }
      //перевірка чи пройшло 10 сек від попереднього критичного удару
      switchCriticalHitFirstFighter = false;
      setTimeout(() => (switchCriticalHitFirstFighter = true), 10000);
    } else if (
      pressedKeys.has(controls.PlayerTwoCriticalHitCombination[0]) &&
      pressedKeys.has(controls.PlayerTwoCriticalHitCombination[1]) &&
      pressedKeys.has(controls.PlayerTwoCriticalHitCombination[2]) &&
      switchCriticalHitSecondFighter
    ) {
      let damage = getDamage(secondFighter, firstFighter) * 2;
      let damageInterest = countDamageInterest(firstFighter, damage);
      if (firstFighterHealth < 0) {
        firstFighterHealthBar.style.width = '0%';
      } else {
        firstFighterHealthBar.style.width = `${(firstFighterHealth -= damageInterest)}%`;
      }
      //перевірка чи пройшло 10 сек від попереднього критичного удару
      switchCriticalHitSecondFighter = false;
      setTimeout(() => (switchCriticalHitSecondFighter = true), 10000);
    }
  });

  document.addEventListener('keyup', (event) => {
    pressedKeys.delete(event.code);
  });

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    function checkWinner() {
      if (secondFighterHealth < 0) {
        resolve(firstFighter);
        clearInterval(timerId);
      } else if (firstFighterHealth < 0) {
        resolve(secondFighter);
        clearInterval(timerId);
      }
    }
    // setInterval продовжує запускатись після виявлення переможця
    let timerId = setInterval(checkWinner, 100)
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage < 0) damage = 0;
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;
  return power;
}

//функція для підрахунку отримання бійцем урону у %
export function countDamageInterest(defender, damage) {
  const oneInterest = defender.health / 100;
  const damageInterest = damage / oneInterest;
  return damageInterest;
}
