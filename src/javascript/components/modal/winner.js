import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function
  
  let fighterProps = {
    title: fighter.name,
    //потрібно, щоб замість тексту заявилось зображення бійця.
    bodyElement: 'Congratulation! You WIN',
    onClose: function () {}
  };

  //bodyElement - це зображення бійця
  return showModal(fighterProps);
}
