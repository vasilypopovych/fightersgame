import { callApi } from '../helpers/apiHelper';

class FighterService {
  #endpoint = 'fighters.json';

  async getFighters() {
    try {
      const apiResult = await callApi(this.#endpoint);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    try {
      this.#endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(this.#endpoint);
      //тут повертається проміс. 
      return await apiResult;
    } catch (error) {
      throw error;
    }

    // todo: implement this method  (DONE)
    // endpoint - `details/fighter/${id}.json`;

  }
}

export const fighterService = new FighterService();
